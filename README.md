# CBIR para la detección de enfermedades en las plantas a partir de sus hojas.

Se implementa una aplicación web orientada a microservicios. Se utilizó a docker como ambiente de contenedor para agrupar los diferentes microservicios y es una aplicación que se puede migrar fácilmente a serveless. Cabe resaltar 

Se implementaron cinco aplicaciones como microservicios que interactuan entre ellas: 

* Frontend: Esta aplicación fue desarrollada con vue.js y contiene la implementación del frontend, aquí se implementó un buscador que permite hacer una consulta en el CBIR adjuntando una imagen de una hoja de alguna planta y muestra al usuario la lista de imágenes recuperadas por el sistema. También se implementó la funcionalidad de poder registrar imágenes en el sistema y añadir su descripción, enfermedad de planta asociada a la imagen.
* Api : Esta aplicación fue desarrollada con Fast Api y Python y contiene la implementación de la lógica del sistema CBIR que guarda imágenes (save images) y el buscador (browser) haciendo uso del resto de microservicios.
* Descriptor: Esta aplicación fue desarrollada con Fast Api, Opencv Python y contiene la lógica que obtiene el descriptor de características del CBIR.
* Storage: Esta aplicación fue desarrollada con Flask y Python y contiene la lógica pura para guardar las imágenes y sus descriptores de características en la base de datos y la lógica pura para hacer la consulta de las imágenes. Como motor de bases de datos se utilizó mongodb.
* Matcher: Esta aplicación fue desarrollada con Fast Api y Python y contiene la lógica utilizada por el CBIR para hacer la comparación entre la imagen consultada y las imágenes guardadas en la base de datos. Las imágenes se comparan por equivalencia en sus descriptores.


Para dar una descripción más detallada se tiene un diagrama de contenedores y un diagrama de secuencias.

* Diagrama de contenedores

![Screenshot](container_diagram.png)

* Diagrama de secuencia

![Screenshot](sequence_diagram.png)

#
## Colaboradores
* Daniel Eduardo Dorado Pérez
* Santiago Andres Ramirez Chavez


