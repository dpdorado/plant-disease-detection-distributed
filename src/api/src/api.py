import os
from typing import List

from fastapi import FastAPI,UploadFile,HTTPException,status,File,Response
from fastapi.middleware.cors import CORSMiddleware
import requests
import urllib
STORAGE_URL = os.environ["STORAGE_URL"] if "STORAGE_URL" in os.environ else "http://localhost:5000/"
DESCRIPTOR_URL = os.environ["DESCRIPTOR_URL"] if "DESCRIPTOR_URL" in os.environ else "http://localhost:8001/"


app = FastAPI()
origins = [
    "http://localhost:8080",
    "http://localhost:8080"
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)
#https://fastapi.tiangolo.com/tutorial/request-files/
@app.get("/")
def hola():
    
    return {"hola":"mensaje de retorno"}

@app.post("/upload_images/")
async def upload_images(disease: str, pfiles: List[UploadFile]= File(...)):
    files = {}
    features_vector = []
    for file in pfiles:
        # Transformo el Archivo de entrada en un formato enviable con "requests"
        file_bytes =  file.file.read()
        files[file.filename] =file_bytes
        # Calculo las caracteristicas de cada imagen con el microservicio "Descriptor Microservice"
        data = {"methods":["method1","method2"]}    
        url_data = urllib.parse.urlencode(data,True)
        descriptor_response= requests.post(DESCRIPTOR_URL+"extract_features?"+url_data, files={'file':file_bytes})
        features_vector.append(descriptor_response.json().get('features'))
    data = {"all_images_features":features_vector,'disease':disease }
    storage_response = requests.post(STORAGE_URL+"upload_image", files=files, data=data)
    return {"status":storage_response.text,"n files upload":len(files)}

@app.post("/browser/")
async def browser(file: UploadFile = File(...)):
    # Calcular el vector de caracteristicas
    descriptor_data = {"methods":["method1","method2"]}
    url_data = urllib.parse.urlencode(descriptor_data,True)
    descriptor_response= requests.post(DESCRIPTOR_URL+"extract_features?"+url_data, files={'file':file.file.read()})
    features = descriptor_response.json().get('features')
    # Consultar al microservicio "storage"
    resp = requests.get(STORAGE_URL+"filter", data={"features":features})
    

    return Response(content=resp.content, media_type=resp.headers.get('content-type', None))
#uvicorn  api:app --reload
#https://www.techiediaries.com/python-requests-upload-file-post-multipart-form-data/


