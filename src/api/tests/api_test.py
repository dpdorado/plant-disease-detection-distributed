import requests
from requests_toolbelt.multipart.encoder import MultipartEncoder
from requests_toolbelt import MultipartDecoder
import re
API_URL = "http://localhost:8000/"


def browser_test():
    filename = "download_1.jpeg"
    resp = requests.post(API_URL+"browser",files={"file": ("file", open(filename, "rb"), "image/jpeg")})
    print(type(resp))
    print(dir(resp))
    decoder = MultipartDecoder(resp.content,resp.headers.get('content-type', None))
    
    splitregex = re.compile(';\\s*')

    
    for part in decoder.parts:
        disp = part.headers[b'Content-Disposition'].decode('utf-8')
    
        dispositions = splitregex.split(disp)
        filename = None
        for disp in dispositions:
            if disp.startswith("filename="):
                filename = disp[10:len(disp)-1]
        wfile = open(filename,"wb")
        wfile.write(part.content)
        wfile.close()
'''
    #return {"filename": file.filename,"dir":"Downloaded {} bytes in {} parts".format(i, len(decoder.parts))}

'''
if  __name__ == "__main__":
    browser_test()
