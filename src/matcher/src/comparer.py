import numpy as np
import cv2

class Comparer:
    def __init__(self):
        # store the number of bins for the 3D histogram		
        pass

    def match(self,features_1, features_2, eps):           
        return self.Chi2(features_1, features_2, eps)
    
    # 1. comperer chi2
    def Chi2(self,x,y,eps):
        # compute the chi-squared distance
        d = 0.5 * np.sum([((a - b) ** 2) / (a + b + eps)
            for (a, b) in zip(features_1, features_2)])

        # return the chi-squared distance
        return d

    # 2. comperer euclidean distance
    def euclidean_distance(self,x,y):               
        return sqrt(sum([pow(a-b,2) for (a, b) in zip(x, y)]))        
    
    # 3. comperer manhattan distance
    def manhattan_distance(self,x,y):

        ''' return manhattan distance between two lists '''

        return sum(abs(a-b) for a,b in zip(x,y))
                
    # 4. comperer minkowski distance
    def minkowski_distance(self,x,y,p_value):

        ''' return minkowski distance between two lists '''

        return self.nth_root(sum(pow(abs(a-b),p_value) for a,b in zip(x, y)),
           p_value)

    def nth_root(self,value, n_root):

        ''' returns the n_root of an value '''

        root_value = 1/float(n_root)
        return round (Decimal(value) ** Decimal(root_value),3)

    # 5. comperer cosine similarity
    def cosine_similarity(self,x,y):

        ''' return cosine similarity between two lists '''

        numerator = sum(a*b for a,b in zip(x,y))
        denominator = self.square_rooted(x)*self.square_rooted(y)
        return round(numerator/float(denominator),3)

    def square_rooted(self,x):

        ''' return 3 rounded square rooted value '''

        return round(sqrt(sum([a*a for a in x])),3)

    # 6. comperer jaccard similarity
    def jaccard_similarity(self,x,y):

        ''' returns the jaccard similarity between two lists '''

        intersection_cardinality = len(set.intersection(*[set(x), set(y)]))
        union_cardinality = len(set.union(*[set(x), set(y)]))
        return intersection_cardinality/float(union_cardinality)
