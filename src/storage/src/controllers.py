from settings import mongo, get_env
from utils import allowed_file,create_unique_filename,load_image_in_base64
from werkzeug.utils import secure_filename
import requests
import urllib
import json
import os
def upload_images(images, all_features, disease):
    # iterate files and features and save
    for [image],features in zip(images,all_features):
        
        if not(image and allowed_file(image.filename)):
           raise Exception("Allowed image types are -> png, jpg, jpeg")
        # save image file
        filename = secure_filename(image.filename)
        filename = create_unique_filename(filename)
        image.save(os.path.join(get_env('UPLOAD_FOLDER'), filename))
        #save features on db
        mongo.db.images.insert_one({"filename":filename,"features":features,'disease':disease})        

def match(features1,features2,p=0.8):
    data =  {"methods":["method1","method2"],"features_1":features1,"features_2":features2}
    #data = json.dumps(data).encode('utf8')
    result = -1
    try:
        response =  requests.get(get_env("MATCHER_URL")+"match",params=data)
        result = response.json()
    except:
        result =match(features1,features2)
    return result
def filter_images(query_features,limit=10):
    images = []
    for image in mongo.db.images.find():
        image['match'] = match(image["features"],query_features)
        if image['match'] != -1:
            images.append(image)
    images.sort(reverse=True,key=lambda x: x['match'])
    return  {str(i)+'-'+image['disease']: (str(i), load_image_in_base64(get_env('UPLOAD_FOLDER'),image['filename']))  for i,image in enumerate(images[:limit]) }
    
