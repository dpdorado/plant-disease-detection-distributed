from flask import safe_join
from datetime import datetime
from random import randint as rand
import base64
ALLOWED_EXTENSIONS = set(['png', 'jpg', 'jpeg', 'gif'])
def extract_extension(filename):
    if '.' in filename:
        return filename.rsplit('.', 1)[1].lower()
def allowed_file(filename):
    
    return extract_extension(filename) in ALLOWED_EXTENSIONS
def create_unique_filename(base_filename):
    if '.' in base_filename:
        name,extension = base_filename.rsplit('.', 1)
        extension.lower()
    else:
        name = base_filename
        extension = ""
    name = "CBIR-APP"
    extra_name = datetime.now()
    extra_name = extra_name.strftime("%d-%m-%Y %H-%M-%S") + str(rand(99,999999))
    return name+extra_name+"."+extension

def load_image_in_base64(folder,image_name):
    path = safe_join(folder,image_name)
    print("loading "+path)
    binary_image = open(path,'rb').read()
    base64_bytes = base64.b64encode(binary_image)
    return  base64_bytes.decode('ascii')