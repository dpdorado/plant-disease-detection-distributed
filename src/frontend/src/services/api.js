import axios from 'axios'
import MultipartDecoder from './multipart'

const API_URL = (process.env.API_URL ) ?  process.env.API_URL : "http://localhost:8000/"

export function search(image,func_resolve){
    let data = new FormData()
    data.append('file',image)
    axios({ 
      method  : 'post', 
      url : API_URL+'browser', 
      data : data, 
    }) .then(
        response => (func_resolve(manage_browser_response(response)))
    ).catch(
        error => console.log(error)
    )
}

export function uppload(images,disease){
    let data = new FormData()
    for(const image of images){
        data.append("files",image)
    }
    data.append("disease",disease)
    axios({ 
      method  : 'post', 
      url : API_URL+'upload_images', 
      data : data, 
    }) .then(
        response => console.log(response.data)
    ).catch(
        error => console.log(error)
    )
}



function manage_browser_response(response) {
    const header = response.headers["content-type"]
    var decoder = new MultipartDecoder(response.data,header)
    return decoder.parts
    
    
}