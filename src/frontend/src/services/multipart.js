
function encodeWith(str,encoding){
    return str //TODO -- Encode with other formats
}
export default class MultipartDecoder{
    constructor(content,mimetype,encoding='utf-8'){
        
        this.boundary = MultipartDecoder.__getBoundary(mimetype)
        
        this.parts = []
        this.encoding = encoding
        this.parse(content)
    }

    static __getBoundary(header){
        if(header.split('/')[0].toLowerCase() != 'multipart'){
            throw "Unexpected mimetype in content-type: '"+mimetype+"'"
        }
        var items = header.split(';')
        if(!items)
            return ""

        for(let i=0;i<items.length;i++){
            var item = (new String(items[i])).trim()
            if(item.indexOf('boundary') >= 0){
                var k = item.split('=')
                return encodeWith((new String(k[1])).trim(),this.encoding)
            }
        }
        return "";
    }

    parse(content){
        const boundary = '--'+this.boundary
        const split = '\r\n--'+ this.boundary
        const unprocessed_parts =content.split(split)

        this.parts = []
        for(const unprocessed_part of unprocessed_parts){
            if(this.__testPart(unprocessed_part)){
                this.parts.push(this.__processPart(unprocessed_part,boundary))
            }
        }
    }
    __testPart(part){
        return (part != '' &&
                part != '--' &&
                part != '\r\n' &&
                part.slice(0,4)!= '--\r\n' )
    }
    __fixFirstPart(part, boundary_marker){
        const bm_len = boundary_marker.length
        if (boundary_marker == part.slice(0,bm_len))
            return part.slice(bm_len)
        else
            return part
    }
    __processPart(part,boundary){
        
        const fixed = this.__fixFirstPart(part,boundary)
        return new  BodyPart(fixed,this.encoding)
    }
}

class BodyPart{
    constructor(content,encoding){
        this.encoding = encoding
        const split = '\r\n\r\n'
        let point = content.indexOf(split)
        if(point == -1){
            throw  'content does not contain CR-LF-CR-LF'
        }
        const first = content.slice(0,point)
        this.content = content.slice(point+split.length)
        if (first != '')
                this.headers = this.__header_parser(first.replace(/^\s+/,""), encoding)// left trim with replace(/^\s+/,"")
      
    }
    __header_parser(string,encoding){
        // TODO decode String  with encoding code
        
        let headers = {}
        const parts = string.split("; ")
        
        for(let part of parts){
            let split = ": "
            let  point  = part.indexOf(split)
            if(point == -1 ){
                split = "="
                point  = part.indexOf(split)
            }       
            const key = part.slice(0,point)
            const value = part.slice(point+split.length)
            if(value.startsWith('"') && value.endsWith('"')) //TODO add ', `
                value = value.slice(1,value.length-1)
            headers[key] = value
            
        }
        return headers
    }    

}  