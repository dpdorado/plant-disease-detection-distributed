import Vue from 'vue'
import router from './router'
import App from './App.vue'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  template: '<App/>',
  components: { App }
})