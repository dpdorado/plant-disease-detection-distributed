import Vue from 'vue'

import Router from 'vue-router'
import Search from "@/views/Search.vue"
import UpploadImages from "@/views/UpploadImages.vue"



const routes = [
  {
    path: "/",
    name: "Search",
    component: Search,
  },
  {
    path: "/uppload",
    name: "Uppload",
    component: UpploadImages,
  },
]
Vue.use(Router)

export default new Router({
    routes: routes
})