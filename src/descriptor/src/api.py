import os
from typing import List

from fastapi import FastAPI,UploadFile,HTTPException,status,File,Response
from fastapi.middleware.cors import CORSMiddleware
import requests
from fastapi import FastAPI, Query
from .descriptor import Descriptor
STORAGE_URL = os.environ["STORAGE_URL"] if "STORAGE_URL" in os.environ else "http://localhost:5000/"


app = FastAPI()
origins = [
    "http://api:8000",
    'http://localhost:8000',
]
app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=False,
    allow_methods=["*"],
    allow_headers=["*"],
)
#https://fastapi.tiangolo.com/tutorial/request-files/
@app.get("/")
def hola():
    
    return {"hola":"mensaje de retorno"}

@app.post("/extract_features")
async def extract_features(methods : List[str] = Query(None),file: UploadFile = File(...)):
    print(methods)
    descriptor = Descriptor((8,12,3))
    features=  descriptor.describe(file.file)
    return {"features":tuple(map(lambda x:float(x),features))}